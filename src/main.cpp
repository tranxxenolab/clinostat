// ALWAYS CHECK THAT THE DIP SWITCHES ARE SET TO WHAT WE
// WANT THE MICROSTEPPING TO BE, BELOW IN SETUP

// MOTOR NOTES
//
// The 42mm NEMA17 motors do not like being run at 12V.
// Run at least 15V or so.
// Motor that we use is a 17HS16
//
// The NEMA17 pancake steppers, however, should be run
// at 12V, not 24V.
//

/*
 ********* TODO **********
 * 
 * * Look here for bi-directional level shifting: https://learn.sparkfun.com/tutorials/bi-directional-logic-level-converter-hookup-guide
 * * Need to split everything out into tasks, use event groups and delays to ensure things get 
 *    scheduled properly. Look at https://microcontrollerslab.com/freertos-event-groups-tasks-synchronization-example-arduino/
 *    and https://microcontrollerslab.com/arduino-freertos-queues-create-read-write-examples/
 *    and example on https://esp32.com/viewtopic.php?t=8944
 * * Ensure that ADC averages over some amount of time
 * * Look into setting the datetime using NTP
 * * Setup preferences using Preferences library (things like saving the speed, etc): 
 *    http://www.iotsharing.com/2017/06/how-to-use-preferences-to-backup-data-in-main-flash-when-power-off.html
 * * Use semaphores to lock access to variables that could be read across cores
 * * Maybe create REST service on my server that the ESP could talk to
 * * Look into JSON serialization for settings and preferences too
 * * Constructor for OLED screens: U8G2_SSD1306_128X64_NONAME_1_HW_I2C u8g2(U8G2_R0);
 * * Look into saving data to internal flash, using LittleFS
 * * Look into moving to a different IDE: https://randomnerdtutorials.com/vs-code-platformio-ide-esp32-esp8266-arduino/
 * 
 */

 /*
 ********* NOTES **********
 * 
 * * The cheap "KMR-1.8" screens use the ST7735 driver and have RGB order as "TFT_RGB"
 * 
 */


/**********  INCLUDES  **********/
#include <Arduino.h>

// For logging capabilities "with minimal performance cost"
// See: https://github.com/thijse/Arduino-Log/
#include <ArduinoLog.h>

// For I2C
#include <Wire.h>

// For acceleromter board
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>

// For Wifi
#include <WiFi.h>

// For web server
#include <WebServer.h>
#include <ArduinoJson.h>

// For the screen
#include <TFT_eSPI.h> // Graphics and font library for ST7735 driver chip
#include <SPI.h>
#include <U8g2lib.h>

// For the ADC
#include <driver/adc.h>

// For time setting
//#include <time.h>
//#include <sys/time.h>
//#include <esp_sntp.h>
//#include <Arduino.h>

// Gotten time sync info from https://github.com/gitpeut/esp_sntp
#include <time.h>
#include "lwip/err.h"
#include "lwip/apps/sntp.h"
//#include "lwip/include/apps/sntp/sntp.h"

// Low overhead circular buffer implementation
#include <CircularBuffer.h>

// Our own headers
#include "wifi_settings.h" // Our header file with the variables defined in there
#include "settings.h" // Our own settings

// This is needed to get rid of the compiler issues
// See https://community.platformio.org/t/identifier-is-undefined-setenv-tzset/16162/2
_VOID      _EXFUN(tzset,	(_VOID));
int	_EXFUN(setenv,(const char *__string, const char *__value, int __overwrite));

/**********  GLOBAL DEFINITIONS  **********/

// Using the Adafruit library for reading from the accelerometer board
Adafruit_MPU6050 mpu;
sensors_event_t accelValues, gValues, tempValue;

// Storing our accelerometer data
CircularBuffer<float, 10> accelXBuffer;
CircularBuffer<float, 10> accelYBuffer;
CircularBuffer<float, 10> accelZBuffer;
float accelXAvg = 0.0;
float accelYAvg = 0.0;
float accelZAvg = 0.0;
  

// What is our log level?
//#define log_level LOG_LEVEL_VERBOSE

// Define the output pins
// D0, D1 imply that we're doing this on an ESP8266 or something of that sort
// Can easily switch out to an Arduino just by changing the pins to something like
// 2 or 3
// Or an ESP32 as well
#define dirPin 17   // connect to DIR+ on motor driver
#define stepPin 16  // connect to PUL+ on motor driver
#define potPin 34   // for our speed control
#define motorEnablePin 15 // for the SPDT switch that determines whether the motor is enabled or not
#define powerLEDPin 4    // for green power LED
#define errorLEDPin 27    // for red error LED
#define pwmPin 25 // for the pump PWM

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

// 35 RST 32 DC 23 MOSI 18 SCK 5 SS
// Define SPI pins as SPI.begin(CLK, MISO, MOSI, SS);

/*
   Switches for microstepping
   !!!VALID FOR TB6600 MOTOR CONROL BOARD ONLY!!!
   something like this:
   https://www.amazon.com/TB6600-Stepper-Driver-Controller-tb6600/dp/B07S64MBTR/

   See also:
   https://www.makerguides.com/tb6600-stepper-motor-driver-arduino-tutorial/

   200  ON  ON  OFF
   400  ON  OFF ON
   800  ON  OFF OFF
   1600 OFF ON  OFF
   3200 OFF OFF ON
   6400 OFF OFF OFF
*/
// Are we microstepping?
bool microstepping = HIGH;

// Setup enum to make the code easier to read
// ALSO CHANGE THIS WHEN YOU CHANGE THE MICROSTEP STATE
enum MircrostepStates {
  MICRO_200 = 1,
  MICRO_400 = 2,
  MICRO_800 = 4,
  MICRO_1600 = 8,
  MICRO_3200 = 16,
  MICRO_6400 = 32
};
MircrostepStates microstepState = MICRO_800;

// How many steps per revolution? Get this from the datasheet
// (360 / angle per step)
long stepsPerRevolution = 200;

// What is the delay time? This will change depending on desired speed and
// microstepping state and will be calculated below
uint32_t delayTimeMicros = 500; // delay time in microseconds

// What is our desired RPMs?
long rpms = 60;

// Is the motor enabled?
volatile boolean motorEnabled = false;
boolean motorEnabledChanged = false;
// What's the value of the switch
boolean motorPinVal;

// PWM channel infos
uint8_t pwmChannel = 0;
uint16_t pwmFrequency = 5000;
uint8_t pwmResolution = 10;
uint16_t PUMP_RUN_FREQUENCY = 5000; // How often to run the pump, in milliseconds

// Create our tasks
TaskHandle_t stepperTask;
TaskHandle_t processingTask;
TaskHandle_t displayTask;
TaskHandle_t mpuTask;
TaskHandle_t pwmTask;
TaskHandle_t webServerTask;

// Web server
WebServer server(WEB_SERVER_PORT);

// JSON data buffer
StaticJsonDocument<250> jsonDocument;
char jsonBuffer[250];

// TFT library
TFT_eSPI tft = TFT_eSPI();  // Invoke library, pins defined in User_Setup.h

// OLED for ESP32
U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* clock=*/ SCL, /* data=*/ SDA, /* reset=*/ U8X8_PIN_NONE);   // All Boards without Reset of the Display
char buf[21];

// What are the states for the screen?
enum DisplayState {
  STATE_INTRO     = 0,
  STATE_IP        = 1,
  STATE_READINGS  = 2
};
DisplayState currentDisplayState = STATE_INTRO;

// Working with potentiometer and speed control
double potValue = 0;
int speedPot = rpms;
int oldSpeedPot = speedPot;
//boolean speedPotChanged = false;

// RTOS definitions
SemaphoreHandle_t sema_motorEnabled;
SemaphoreHandle_t sema_delayTimeMicros;
SemaphoreHandle_t sema_mpuValues;
TickType_t semaphoreTicksToWait = 12;

// Time variables
char strftime_buf[64];

// Functions
void setPowerLED();
void clearPowerLED();
void setErrorLED();
void clearErrorLED();
void checkMotorEnabled();
void readSpeedPot();
void setupMPU();
void stepperControl(void * pvParameters);
void processingControl(void * pvParameters);
void displayControl(void * pvParameters);
void mpuControl(void * pvParameters);
void webServerControl(void * pvParameters);
void pwmControl(void * pvParameters);
void u8g2_prepare();
void getMPUValues();
int getDelayMicros(long rpm, long stepsPerRev, int microState);
int getDelayMicros(long rpm, long stepsPerRev);
void setupRouting();
void getAccelValues();

void ntp_setup(bool sync);
void ntp_waitforsync();

/**********  FUNCTION DEFINITIONS  **********/

/*
 * Initialize ESP32
 */
void setup() {
 // Setup logging
  Serial.begin(SERIAL_BAUD);
  while (!Serial && !Serial.available()) {}
  Log.begin(log_level, &Serial);

  Log.notice("    " CR);
  Log.notice("******                           ******" CR);
  Log.notice("****** XENOLOGICAL ENTANGLEMENTS ******" CR);
  Log.notice("******       001: EROMATASE      ******" CR);
  Log.notice("******                           ******" CR);
  
  Log.verbose("Beginning setup" CR);

  // Setup the pins
  Log.notice("Setting up pins..." CR);
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  digitalWrite(dirPin, LOW);

  pinMode(motorEnablePin, INPUT);

  pinMode(powerLEDPin, OUTPUT);
  pinMode(errorLEDPin, OUTPUT);
  digitalWrite(powerLEDPin, LOW);
  digitalWrite(errorLEDPin, LOW);

  // Setup pins for PWM
  if (HAVE_PUMP) {
    pinMode(pwmPin, OUTPUT);
    digitalWrite(pwmPin, LOW);

    ledcSetup(pwmChannel, pwmFrequency, pwmResolution);
    ledcAttachPin(pwmPin, pwmChannel);
    Log.notice("Setup pump pin and PWM channel" CR);
  }

  Log.notice("Pins set" CR);

  // Begin by turning the power LED on
  setPowerLED();

  // Output SPI pin info
  //outputSPIPins();

  // Output I2C pin info
  //outputI2CPins();

  // Setup RTOS variables
  Log.notice("Setting up semaphores, queues, event groups..." CR);
  sema_motorEnabled = xSemaphoreCreateMutex();
  xSemaphoreGive(sema_motorEnabled);
  sema_delayTimeMicros = xSemaphoreCreateMutex();
  xSemaphoreGive(sema_delayTimeMicros);
  sema_mpuValues = xSemaphoreCreateMutex();
  xSemaphoreGive(sema_mpuValues);
  Log.notice("Semaphores, queues, event groups setup" CR);

  // Setup ADC
  // TODO check on the right channel to be using here
  Log.notice("Setting up ADC..." CR);
  adc1_config_width(ADC_WIDTH_12Bit);
  adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_11db);
  Log.notice("ADC setup" CR);

  if (USE_WIFI) {
    int wifiConnectCount = 0;

    // Setup Wifi
    Log.notice("Setting up WiFi..." CR);
    WiFi.begin(ssid, password);
   
    while (WiFi.status() != WL_CONNECTED) {
      Log.notice("Connecting to WiFi.." CR);
      delay(1000);

      wifiConnectCount += 1;

      if (wifiConnectCount >= WIFI_CONNECT_TRIES) {
        Log.error("Unable to connect to Wifi network" CR);
        setErrorLED();
        USE_WIFI = false;
        SET_TIME = false;
        break;
      }
    }
   
    Log.notice("Connected to the WiFi network" CR);
    Log.notice("This device IP: %s" CR, WiFi.localIP().toString().c_str());  
  } else {
    Log.warning("Not utilizing the Wifi connection" CR);
  }

  if (SET_TIME) {
    //time_t now;
    //now = time(nullptr);
    
    //pftime::configTzTime(TIME_ZONE, 0, NTP_SERVER);
    //printLocalTime();
    ntp_setup(true);

    //Log.notice("Local time is: %s" CR, asctime(localtime(&now)));
    
  }

  // Calculate the delay time given our parameters
  Log.verbose("Reading speed pot and motor enable switch" CR);
  readSpeedPot();
  checkMotorEnabled();
  
  if (HAVE_ACCEL) {
    Log.notice("Starting MPU..." CR);
    if (!mpu.begin()) {
      Log.error("Failed to find MPU6050 chip" CR);
      digitalWrite(errorLEDPin, HIGH);
    } else {
      Log.notice("MPU6050 found" CR);
      setupMPU();
    }
  } else {
    Log.notice("Not utilizing accelerometer" CR);
  }

  if (HAVE_SCREEN) {
    Log.notice("Starting up screen functions..." CR);
    //tft.init();
    //tft.setRotation(0);
    //tft.fillScreen(TFT_BLACK);
    //tft.setTextColor(TFT_GREEN, TFT_BLACK);  // Adding a black background colour erases previous text automatically
  
    Log.notice("Starting up OLED screen functions..." CR);
    /*
     * We seem to have to run things at a super slow clock speed because of running on the second core?
     * Need to look into this more, because it doesn't make any sense to me
     */
    // TODO look into setting this in Wire, and also other options,
    // like increasing the task priority, etc
    u8g2.setBusClock(60000);
    u8g2.begin();
    Log.notice("Screen functions setup" CR);
  } else {
    Log.notice("Not utilizing screen" CR);
  }

  Log.notice("Setting interrupts..." CR);
  attachInterrupt(motorEnablePin, motorEnableChange, CHANGE);

  Log.notice("Starting tasks..." CR);

  xTaskCreatePinnedToCore(
    stepperControl,     /* Task function. */
    "Stepper Control",  /* name of task. */
    10000,        /* Stack size of task */
    NULL,         /* parameter of the task */
    1,            /* priority of the task */
    &stepperTask, /* Task handle to keep track of created task */
    1             /* pin task to core 1 */
  );

  delay(500);


  xTaskCreatePinnedToCore(
    processingControl,     /* Task function. */
    "Processing Control",  /* name of task. */
    2000,        /* Stack size of task */
    NULL,         /* parameter of the task */
    1,            /* priority of the task */
    &processingTask, /* Task handle to keep track of created task */
    0             /* pin task to core 0 */
  );

  delay(500);

  if (HAVE_ACCEL) {
    xTaskCreatePinnedToCore(
      mpuControl,     /* Task function. */
      "MPU Control",  /* name of task. */
      10000,        /* Stack size of task */
      NULL,         /* parameter of the task */
      1,            /* priority of the task */
      &mpuTask, /* Task handle to keep track of created task */
      0             /* pin task to core 0 */
    );
  }

  if (HAVE_SCREEN) {
    xTaskCreatePinnedToCore(
      displayControl,     /* Task function. */
      "Display Control",  /* name of task. */
      10000,        /* Stack size of task */
      NULL,         /* parameter of the task */
      12,            /* priority of the task */
      &displayTask, /* Task handle to keep track of created task */
      0             /* pin task to core 0 */
    );
  }

  if (USE_WEB_SERVER) {
    xTaskCreatePinnedToCore(
      webServerControl,     /* Task function. */
      "Web Server Control",  /* name of task. */
      10000,        /* Stack size of task */
      NULL,         /* parameter of the task */
      12,            /* priority of the task */
      &webServerTask, /* Task handle to keep track of created task */
      1             /* pin task to core 1 */
    );
  }

  if (HAVE_PUMP) {
    xTaskCreatePinnedToCore(
      pwmControl,     /* Task function. */
      "PWM Control",  /* name of task. */
      10000,        /* Stack size of task */
      NULL,         /* parameter of the task */
      12,            /* priority of the task */
      &pwmTask, /* Task handle to keep track of created task */
      1             /* pin task to core 1 */
    );
  }

  Log.notice("Tasks started" CR);

  delay(500);

  Log.verbose("Ending setup" CR);
}

/*
 *
 * Interrupt routines
 * 
 */
void IRAM_ATTR motorEnableChange() {
  motorEnabledChanged = true;
}

/*
 * Initialize MPU6050 accelerometer, gyro, and temp board
 */
void setupMPU() {
  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);
  Log.verbose("Accelerometer range set to: ");
  switch (mpu.getAccelerometerRange()) {
  case MPU6050_RANGE_2_G:
    Log.verbose("+-2G" CR);
    break;
  case MPU6050_RANGE_4_G:
    Log.verbose("+-4G" CR);
    break;
  case MPU6050_RANGE_8_G:
    Log.verbose("+-8G" CR);
    break;
  case MPU6050_RANGE_16_G:
    Log.verbose("+-16G" CR);
    break;
  }
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);
  Log.verbose("Gyro range set to: ");
  switch (mpu.getGyroRange()) {
  case MPU6050_RANGE_250_DEG:
    Log.verbose("+- 250 deg/s" CR);
    break;
  case MPU6050_RANGE_500_DEG:
    Log.verbose("+- 500 deg/s" CR);
    break;
  case MPU6050_RANGE_1000_DEG:
    Log.verbose("+- 1000 deg/s" CR);
    break;
  case MPU6050_RANGE_2000_DEG:
    Log.verbose("+- 2000 deg/s" CR);
    break;
  }

  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  Log.verbose("Filter bandwidth set to: ");
  switch (mpu.getFilterBandwidth()) {
  case MPU6050_BAND_260_HZ:
    Log.verbose("260 Hz" CR);
    break;
  case MPU6050_BAND_184_HZ:
    Log.verbose("184 Hz" CR);
    break;
  case MPU6050_BAND_94_HZ:
    Log.verbose("94 Hz" CR);
    break;
  case MPU6050_BAND_44_HZ:
    Log.verbose("44 Hz" CR);
    break;
  case MPU6050_BAND_21_HZ:
    Log.verbose("21 Hz" CR);
    break;
  case MPU6050_BAND_10_HZ:
    Log.verbose("10 Hz" CR);
    break;
  case MPU6050_BAND_5_HZ:
    Log.verbose("5 Hz" CR);
    break;
  }
}


/*
 * Set and clear functions for our LEDs
 */
void setPowerLED() {
  digitalWrite(powerLEDPin, HIGH);
}

void clearPowerLED() {
  digitalWrite(powerLEDPin, LOW);
}

void setErrorLED() {
  digitalWrite(errorLEDPin, HIGH);
}

void clearErrorLED() {
  digitalWrite(errorLEDPin, LOW);  
}

/*
 * Stepper control task
 * 
 * Merely sends pulses to move the stepper motor :)
 * 
 * But also does it in a thread-safe way ;)
 * 
 */
void stepperControl(void * pvParameters) {
  Log.notice("Starting stepper control task" CR);

  boolean localMotorEnabled;
  int localDelayTimeMicros;

  for (;;) {
    // Get local versions of global variables
    if (xSemaphoreTake(sema_motorEnabled, semaphoreTicksToWait ) == pdTRUE) {
      localMotorEnabled = motorEnabled;
      xSemaphoreGive(sema_motorEnabled);
    } else {
      Log.error("Unable to get motorEnabled semaphore" CR);
      setErrorLED();
    }

    if (xSemaphoreTake(sema_delayTimeMicros, semaphoreTicksToWait ) == pdTRUE) {
      localDelayTimeMicros = delayTimeMicros;
      xSemaphoreGive(sema_delayTimeMicros);
    } else {
      Log.error("Unable to get delayTimeMicros semaphore" CR);
      setErrorLED();
    }

    //Log.verbose("At end of stepperControl" CR);
    localMotorEnabled = motorEnabled;
    localDelayTimeMicros = delayTimeMicros;
    // Check if the motor is enabled
    if (localMotorEnabled) {
      // Run through our usual loop of stepping
      // TODO do this in terms of ticks rather than in terms of milliseconds
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(localDelayTimeMicros);
      digitalWrite(stepPin, LOW);
      delayMicroseconds(localDelayTimeMicros);
      
    } else {
      // For some reason, we need some sort of delay here,
      // otherwise the logic doesn't work
      delayMicroseconds(localDelayTimeMicros);
    }
  }
}

/*
 * Processing task
 * 
 * Reads sensors, calculates values, and writes information to the screen
 * 
 */
void processingControl(void * pvParameters) {

  Log.notice("Starting processing control task" CR);

  // For delaying until a specified amount of time has passed
  TickType_t xLastWakeTime;
  
  /*
   * This is given in "ticks", so if we divide by the constant portTICK_PERIOD_MS 
   * we can calculate it in milliseconds (given by the first value).
   * So this will wait for 500ms.
   * 
   */
  const TickType_t xFrequency = pdMS_TO_TICKS(500);

  /*
   * Initialise the xLastWakeTime variable with the current time.
   * This will automatically be updated in the loop by vTaskDelayUntil().
   */
  xLastWakeTime = xTaskGetTickCount();
  
  for (;;) {
    //Log.verbose("In loop" CR);

    vTaskDelayUntil(&xLastWakeTime, xFrequency);

    /* Read from the speed pot */
    readSpeedPot();
    //delayTimeMicros = 300;
    Log.verbose("Mapped speed value: %d" CR, speedPot);

    // Run the motor enabled check if there has been a change in the lswitch
    if (motorEnabledChanged) {
      checkMotorEnabled();
      motorEnabledChanged = false;
    }

    //Log.verbose("At end of processingControl" CR);
    /* Get new MPU sensor events with the readings */
    //getMPUValues();
  
    /* Print out the values */
    //Log.notice("Acceleration X: %F, Y: %F, Z: %F m/s^2" CR, a.acceleration.x, a.acceleration.y, a.acceleration.z);
    //Log.notice(a.acceleration.x, CR);
    //Log.notice(", Y: ", CR);
    //Log.notice(a.acceleration.y, CR);
    //Log.notice(", Z: ", CR);
    //Log.notice(a.acceleration.z, CR);
    //Log.notice(" m/s^2", CR);
    //tft.drawCentreString("Speed",64,130,3);
//    tft.setCursor(0, 40);
//    tft.setTextSize(1);
//    String accelText = String("Accel: ");
//    accelText = accelText + a.acceleration.x + " " + a.acceleration.y + " " + a.acceleration.z;
//    tft.print(accelText.c_str());
//    
//    tft.setCursor(0, 130);
//    tft.setTextSize(1);
//    String speedText = String("Speed: ");
//    speedText.concat(speedPot);
//    tft.print(speedText.c_str());

  }
}

/*
 * Display control task
 * 
 * Step through different screens and display data to the screen
 * 
 */
void displayControl(void * pvParameters) {

  // For delaying until a specified amount of time has passed
  TickType_t xLastWakeTime;
  
  /*
   * This is given in "ticks", so if we divide by the constant portTICK_PERIOD_MS 
   * we can calculate it in milliseconds (given by the first value).
   * So this will wait for 200ms.
   * 
   */
  TickType_t xFrequency = pdMS_TO_TICKS(6000);

  /*
   * Initialise the xLastWakeTime variable with the current time.
   * This will automatically be updated in the loop by vTaskDelayUntil().
   */
  xLastWakeTime = xTaskGetTickCount();

  float accelX = 0, accelY = 0, accelZ = 0;
  float temp = 0;

  time_t  rawt;
  struct tm *tinfo;

  for (;;) {
    // Switch through our display states
        
    switch (currentDisplayState) {
      case STATE_INTRO:
        // Display intro screen
        u8g2.clearBuffer();
        u8g2_prepare();
        u8g2.drawStr(0, 0,  "**                 **");
        u8g2.drawStr(0, 10, "**   XENOLOGICAL   **");
        u8g2.drawStr(0, 20, "**  ENTANGLEMENTS  **");
        u8g2.drawStr(0, 30, "** 001: EROMATASE  **");
        u8g2.drawStr(0, 40, "**                 **"); 
        u8g2.sendBuffer();
        if (USE_WIFI) {
          currentDisplayState = STATE_IP;  
        } else {
          currentDisplayState = STATE_READINGS;  
        }
        
        break;
      case STATE_IP:
        // Display wifi info       
        u8g2.clearBuffer();
        u8g2_prepare();
        snprintf(buf, 21, "SSID:");
        u8g2.drawStr(0, 20, buf);
        snprintf(buf, 21, "%s", ssid);
        u8g2.drawStr(0, 30, buf); 
        snprintf(buf, 21, "IP: %s", WiFi.localIP().toString().c_str());
        u8g2.drawStr(0, 40, buf); 
        u8g2.sendBuffer();
        currentDisplayState = STATE_READINGS;
        break;
      case STATE_READINGS:
        xFrequency = pdMS_TO_TICKS(500);
        /* Get new MPU sensor events with the readings */
        u8g2.clearBuffer();
        u8g2_prepare();
        if (xSemaphoreTake(sema_mpuValues, semaphoreTicksToWait ) == pdTRUE) {
          accelX = accelValues.acceleration.x;
          accelY = accelValues.acceleration.y;
          accelZ = accelValues.acceleration.z;
          temp = tempValue.temperature;
          
          xSemaphoreGive(sema_mpuValues);
        } else {
          Log.error("Unable to get mpuValues semaphore in displayTask()" CR);
          setErrorLED();
        }

        if (SET_TIME) {
          time( &rawt );
          tinfo = localtime( &rawt );
          
          strftime(strftime_buf, 21, "%a %b %d %Y", tinfo);
          Log.verbose("%s" CR, strftime_buf);
          snprintf(buf, 21, strftime_buf);
          u8g2.drawStr(0, 0, buf);

          strftime(strftime_buf, 21, "%H:%M:%S", tinfo);
          Log.verbose("%s" CR, strftime_buf);
          snprintf(buf, 21, strftime_buf);
          u8g2.drawStr(0, 10, buf);

          //strftime(strftime_buf, sizeof(strftime_buf), "%A, %B %d %Y %H:%M:%S", &timeinfo);
          //Log.notice("%s", ctime(&rawt));
          //snprintf(buf, 21, ctime(&rawt));
          //u8g2.drawStr(0, 0, buf);
        }

        Log.verbose("Averages: %F %F %F" CR, accelXAvg, accelYAvg, accelZAvg);
        Log.verbose("Actual: %F %F %F" CR, accelX, accelY, accelZ);
        // Commenting out accelerometer output for documentary video shooting
        //snprintf(buf, 21, "Accel:");
        //u8g2.drawStr(0, 20, buf);
        //snprintf(buf, 21, "%.1f %.1f %.1f", accelXAvg, accelYAvg, accelZAvg);
        //u8g2.drawStr(0, 30, buf);
        snprintf(buf, 21, "Temp: %.1f", temp);
        u8g2.drawStr(0, 20, buf);
        u8g2.sendBuffer();
        

        break;
    }

    vTaskDelayUntil(&xLastWakeTime, xFrequency);

  }

}

/*
 * MPU Processing task
 * 
 * Run the code to read from the MPU, and then also
 * calculate the time average of the acceleration values
 * 
 */
void mpuControl (void * pvParameters) {
  Log.notice("Starting mpu processing control task" CR);

  // For delaying until a specified amount of time has passed
  TickType_t xLastWakeTime;
  
  /*
   * This is given in "ticks", so if we divide by the constant portTICK_PERIOD_MS 
   * we can calculate it in milliseconds (given by the first value).
   * So this will wait for 100ms.
   * 
   */
  const TickType_t xFrequency = pdMS_TO_TICKS(100);

  /*
   * Initialise the xLastWakeTime variable with the current time.
   * This will automatically be updated in the loop by vTaskDelayUntil().
   */
  xLastWakeTime = xTaskGetTickCount();

  float tempAccelXAvg = 0.0;
  float tempAccelYAvg = 0.0;
  float tempAccelZAvg = 0.0;
  
  for (;;) {
    getMPUValues();

    tempAccelXAvg = 0.0;
    tempAccelYAvg = 0.0;
    tempAccelZAvg = 0.0;
    using index_t = decltype(accelXBuffer)::index_t;
    for (index_t i = 0; i < accelXBuffer.size(); i++) {
      tempAccelXAvg += accelXBuffer[i];
      tempAccelYAvg += accelYBuffer[i];
      tempAccelZAvg += accelZBuffer[i];
    }

    accelXAvg = tempAccelXAvg / (float)accelXBuffer.size();
    accelYAvg = tempAccelYAvg / (float)accelYBuffer.size();
    accelZAvg = tempAccelZAvg / (float)accelZBuffer.size();

    vTaskDelayUntil(&xLastWakeTime, xFrequency);
  }
}

/*
 * PWM task
 * 
 * Runs the pump every certain number of seconds
 * 
 */
void pwmControl(void * pvParameters) {

  Log.notice("Starting PWM control task" CR);

  // For delaying until a specified amount of time has passed
  TickType_t xLastWakeTime;
  
  /*
   * This is given in "ticks", so if we divide by the constant portTICK_PERIOD_MS 
   * we can calculate it in milliseconds (given by the first value).
   * So this will wait for 200ms.
   * 
   */
  const TickType_t xFrequency = pdMS_TO_TICKS(PUMP_RUN_FREQUENCY);

  /*
   * Initialise the xLastWakeTime variable with the current time.
   * This will automatically be updated in the loop by vTaskDelayUntil().
   */
  xLastWakeTime = xTaskGetTickCount();
  
  for (;;) {
    vTaskDelayUntil(&xLastWakeTime, xFrequency);

    ledcWrite(pwmChannel, 1000);
    delay(25); // needs to be around 100 for 6V on the motors, 25 for 18V
    ledcWrite(pwmChannel, 0);
  }
}

/*
 * Web server control task
 * 
 * Starts the web server and sets it up
 * 
 */
void webServerControl(void * pvParameters) {
  setupRouting();

  for (;;) {
    server.handleClient();
  }

}

void setupRouting() {	 	 
  server.on("/accel", getAccelValues);	 	  	 

  // start server	 	 
  server.begin();	 	 
}

void getAccelValues() {
  
  JsonObject obj = jsonDocument.createNestedObject();
  char accelBuffer[20];
  obj["type"] = "accel";
  snprintf(accelBuffer, 20, "%.1f,%.1f,%.1f", accelXAvg, accelYAvg, accelZAvg);
  obj["value"] = accelBuffer; 
  serializeJson(jsonDocument, jsonBuffer);
  server.send(200, "application/json", jsonBuffer); 
}

/*
 * loop() doesn't need to have anything in it since 
 * we're running everything with Tasks
 * 
 */
void loop() {}

/*
 * Update the delayTime between steps
 * 
 */
void updateMicros() {
  if (xSemaphoreTake(sema_delayTimeMicros, semaphoreTicksToWait ) == pdTRUE) {
    if (microstepping) {
      delayTimeMicros = getDelayMicros(speedPot, stepsPerRevolution, microstepState);
    } else {
      delayTimeMicros = getDelayMicros(speedPot, stepsPerRevolution);
    }
    
    xSemaphoreGive(sema_delayTimeMicros);
  } else {
    Log.error("Unable to get delayTimeMicros semaphore in updateMicros()" CR);
    setErrorLED();
  }

  Log.verbose("delayTimeMicros: %d" CR, delayTimeMicros);
}

/*
 * Check if the motor is enabled or not
 */
void checkMotorEnabled() {
  /* Read the motor switch value */
  motorPinVal = digitalRead(motorEnablePin);
  if (xSemaphoreTake(sema_motorEnabled, semaphoreTicksToWait ) == pdTRUE) {
    if (motorPinVal == HIGH) {
      motorEnabled = true;
    } else {
      motorEnabled = false;
    }
    xSemaphoreGive(sema_motorEnabled);
  }
}

/*
 * Read the current values from the MPU in a thread-safe way
 */
void getMPUValues() {
   if (xSemaphoreTake(sema_mpuValues, semaphoreTicksToWait ) == pdTRUE) {
    mpu.getEvent(&accelValues, &gValues, &tempValue);
    
    accelXBuffer.unshift(accelValues.acceleration.x);
    accelYBuffer.unshift(accelValues.acceleration.y);
    accelZBuffer.unshift(accelValues.acceleration.z);
    
    xSemaphoreGive(sema_mpuValues);
  } else {
    Log.error("Unable to get mpuValues semaphore in getMPUValues()" CR);
    setErrorLED();
  }

//  using index_t = decltype(accelXBuffer)::index_t;
//  for (index_t i = 0; i < accelXBuffer.size(); i++) {
//    Log.verbose("%F", accelXBuffer[i]);
//  }
}

/*
 * Read the potentiometer that controls speed
 */
void readSpeedPot() {
  double sumValue = 0.0;
  double result = 0.0;
  double minValue = 5000.0; // 5000 is good for LOG_LEVEL_WARNING
  double maxValue = 0.0;
  int max_reads = 8;

  for (int i = 0; i <= max_reads; i++) {
    potValue = (double)analogRead(potPin);
    //Log.verbose("potValue is %D" CR, potValue);
    sumValue += potValue;
    if (potValue > maxValue) maxValue = potValue;
    if (potValue < minValue) minValue = potValue;
    delay(10);
  }
  result = (sumValue - maxValue - minValue)/6;
  //Log.verbose("result is %D" CR, result);
  
  
  
  //Log.verbose("potValue: %d" CR, potValue);
  //Log.verbose("adc value for pin 34 is %d" CR, analogRead(34));

  speedPot = map(result, 0, 4096, 350, 100);
  if ((speedPot <= (oldSpeedPot - 5)) || (speedPot >= (oldSpeedPot + 5))) {
    updateMicros();
    oldSpeedPot = speedPot;
  }
}

// Get the desired delay based on these parameters
int getDelayMicros(long rpm, long stepsPerRev, int microState) {
  //Log.verbose("Values: %d, %d, %d" CR, rpm, stepsPerRev, microState);

  float delayTimeFloat = 60L * 1000L * 1000L / (2 * rpm * stepsPerRev * microState);

  //Log.verbose("delayTimeFloat: %F" CR, delayTimeFloat);
  long delayTime = round(delayTimeFloat);

  return delayTime;
}


// Get the desired delay based on these parameters
int getDelayMicros(long rpm, long stepsPerRev) {
  //Log.verbose("Values: %d, %d" CR, rpm, stepsPerRev);

  float delayTimeFloat = 60L * 1000L * 1000L / (2 * rpm * stepsPerRev);

  //Log.verbose("delayTimeFloat: %F" CR, delayTimeFloat);
  long delayTime = round(delayTimeFloat);

  return delayTime;
}

void u8g2_prepare(void) {
  u8g2.setFont(u8g2_font_6x10_tf);
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
}

/*
 * Time methods
 * Taken from https://github.com/gitpeut/esp_sntp/blob/master/simplesntp_esp32/simplesntp_esp32.ino
 */
void ntp_setup(bool sync) {
  Log.notice("Setting up SNTP..." CR);
  setenv("TZ", TIME_ZONE, 1);  
  tzset();

  sntp_stop();
  sntp_setoperatingmode(SNTP_OPMODE_POLL);
  sntp_setservername(0, NTP_SERVER);

  sntp_init();
  if (sync) {
    ntp_waitforsync();
  }
}

void ntp_waitforsync() {
  Log.notice("Waiting for time synchronization..." CR);

  time_t  rawt;
  struct tm *tinfo;
  int y;
  int count = 0;
  int max_tries = 20;
    
   for(;;){
     time( &rawt );
     tinfo = localtime( &rawt );
     
     //Log.notice( "Waiting for ntp sync, time is now " );
     //Log.notice("%s" CR, ctime(&rawt));

     y = tinfo->tm_year + 1900;
     if ( y > 2000 ) {
      //Log.notice( "" );
      Log.notice("Time synced, time is now %s", ctime(&rawt));
      break;
     }

     if (count >= max_tries) {
      Log.warning("Unable to set time using NTP server. Disabling time capabilities." CR);
      setErrorLED();
      SET_TIME = false;
      break; 
     } else {
      count += 1;
     }
     
     delay(500); 
   }
}

/**
   https://techtutorialsx.com/2017/05/07/esp32-arduino-passing-a-variable-as-argument-of-a-freertos-task/
   https://randomnerdtutorials.com/esp32-dual-core-arduino-ide/

   Working with semaphroes https://www.youtube.com/watch?v=k_D_Qu0cgu8

  "Btw, I had a watchdog problem with core0 in Arduino.
  I had to add these 3 lines to the for(;;) loop:
    TIMERG0.wdt_wprotect=TIMG_WDT_WKEY_VALUE;
    TIMERG0.wdt_feed=1;
    TIMERG0.wdt_wprotect=0;"

   Also look at this, for queues: https://www.youtube.com/watch?v=ywbq1qR-fY0

   And this, for NTP: https://lastminuteengineers.com/esp32-ntp-server-date-time-tutorial/

   Take a look at this thread for a long example of tasks, queues, semaphores, event groups, etc: https://esp32.com/viewtopic.php?t=8944
*/