/*
 * Put here specific configuration information
 */

// Include this for the log level definitions
#include <ArduinoLog.h>

// What is our log level?
#define log_level LOG_LEVEL_NOTICE

// What baud rate do we want for the serial console?
#define SERIAL_BAUD 115200

// What's our clinostat ID?
#define CLINOSTAT_ID 0

// What kind of clinostat are we running? 
enum ClinostatKinds {
  CLINOSTAT_LAB = 0,
  CLINOSTAT_SACCULAR_FOUNT = 1
};

ClinostatKinds clinostatKind = CLINOSTAT_SACCULAR_FOUNT;

// Do we have a screen?
boolean HAVE_SCREEN = false;

// Do we have an accelerometer board?
boolean HAVE_ACCEL = false;

// Do we have the pump?
boolean HAVE_PUMP = false;

// Are we using Wifi?
boolean USE_WIFI = false;
#define WIFI_CONNECT_TRIES 20

// Time server definitions
boolean SET_TIME = false;
#define TIME_ZONE "CET-1CEST"
//#define NTP_SERVER "pool.ntp.org"
char NTP_SERVER[] = "2.europe.pool.ntp.org";

//const char* ntpServer = "pool.ntp.org";
//const long  gmtOffset_sec = 3600;
//const int   daylightOffset_sec = 3600;

void IRAM_ATTR motorEnableChange();

// For web server
boolean USE_WEB_SERVER = false;
#define WEB_SERVER_PORT 80